import kotlin.math.min

import kotlin.math.sqrt

//fun main(){
//    var p1 = Point()
//    p1.x = 1.0
//    p1.y = 4.0
//
//    var p2 = Point()
//    p2.x = 5.0
//    p2.y = 7.0
//
//
//    println(p1.toString())
//    println(p1 == p2)
//    println(p1.negativePoint())
//    println(p1.twoPoint(p2))
//
//}
//class Point{
//    var x: Double = 0.0
//    var y: Double = 0.0
//    var x1: Double = 0.0
//    var y1: Double = 0.0
//    var Length: Double = 0.0
//
//    override fun toString(): String {
//        return "$x, $y"
//    }
//
//    override fun equals(other: Any?): Boolean {
//        if(other is Point)
//            if (x == other.x && y == other.y)
//                return true
//        return false
//    }
//
//    fun negativePoint(): String {
//        x1 = x * (-1)
//        y1 = y * (-1)
//        return "$x1, $y1"
//
//    }
//    fun twoPoint(other: Any?):Double {
//        if(other is Point)
//            Length = sqrt((other.x - x) * (other.x - x) + (other.y - y) * (other.y - y))
//        return Length
//
//
//    }
//
//
//
//
//}


fun main(){
    var f1 = Fraction()
    f1.numerator = 25
    f1.denominator = 12

    var f2 = Fraction()
    f2.numerator = 6
    f2.denominator = 8

    println(f1 == f2)
    println(f1. toString())
    println(f1.multiply(f2))
    println(f1.devide(f2))
    println(f2.FractionReduction())
    println(f1.AssemblingFractions(f2))

}
class Fraction {
    var numerator: Int = 0
    var denominator: Int = 0
    var multiN: Int = 0
    var multiD: Int = 0
    var devidedN: Int = 0
    var devidedD: Int = 0
    var minimal: Int = 0
    var GCD: Int = 0
    var numerator1: Int = 0
    var denominator1: Int = 0
    var LCM: Int = 0

    override fun equals(other: Any?): Boolean {
        if (other is Fraction) {
            if (numerator * other.denominator / denominator == other.numerator)
                return true
        }
        return false

    }

    override fun toString(): String {
        return "$numerator / $denominator "

    }

    fun multiply(other: Any?): String {
        if (other is Fraction) {
            multiN = numerator * other.numerator
            multiD = denominator * other.denominator

        }
        return "$multiN / $multiD"
    }

    fun devide(other: Any?): String {
        if (other is Fraction) {
            devidedN = numerator * other.denominator
            devidedD = denominator * other.numerator
        }
        return "$devidedN / $devidedD"
    }

    fun FractionReduction(): String {
        minimal = min(numerator, denominator)
        for (i in 1..minimal) {
            if (numerator % i == 0 && denominator % i == 0) {
                GCD = i
                numerator1 = numerator / GCD
                denominator1 = denominator / GCD
            }

        }
        return "$numerator1 / $denominator1"


    }

    fun AssemblingFractions(other: Any?): String {
        if (other is Fraction) {
            minimal = min(denominator, other.denominator)
            for (i in 1..minimal) {
                if (other.denominator % i == 0 && denominator % i == 0) {
                    GCD = i
                    LCM = (denominator * other.denominator) / GCD
                    numerator1 = numerator * (LCM / denominator) + other.numerator * (LCM / other.denominator)
                    denominator1 = LCM
                }
            }
        }
        return "$numerator1 / $denominator1"


    }
}